package main

import (
	"gitlab.com/sirlath/go-js-dom"
	"strings"
)

var dirtyElements = []string{
	`a[href*="Apple"]`,
	`a[href*="Homekit"]`,
	`a[href*="iPhone"]`,
}

func main() {
	dom.GetWindow().
		MutationObserver(mutationDetected).
		Observe(dom.GetWindow().Document(), dom.MutationObserverConfig{
			ChildList: true,
			SubTree:   true,
		})
}

func mutationDetected(mutations []*dom.MutationRecord) {
	for _, mutation := range mutations {
		if strings.EqualFold(mutation.Type(), "childList") && len(mutation.AddedNodes()) > 0 {
			liberate()
			break
		}
	}
}

func liberate() {

	// Liberate the menu!
	forEach(
		dom.GetWindow().Document().QuerySelectorAll(`li.root.section a[href="#"]`),
		func(i dom.Element) {
			if isApple(i) {
				parent := i.ParentElement()
				if parent != nil {
					removeElement(parent)
				}
			}
		},
	)

	// Liberate the burger!
	forEach(
		dom.GetWindow().Document().QuerySelectorAll(`.burger-nav li>a>span`),
		func(i dom.Element) {
			if isApple(i) {
				parent := i.ParentElement().ParentElement()
				if parent != nil {
					removeElement(parent)
				}
			}
		},
	)

	forEach(
		dom.GetWindow().Document().QuerySelectorAll(`.checkbox-wrap._small .checkbox-label`),
		func(i dom.Element) {
			if isApple(i) {
				parent := i.ParentElement()
				if parent != nil {
					removeElement(parent)
				}
			}
		},
	)

	forEach(
		dom.GetWindow().Document().QuerySelectorAll(strings.Join(dirtyElements, ", ")),
		func(i dom.Element) {
			if isProductTipSliderAnchor(i) {
				parent := queryParent(i, func(object dom.Element) bool {
					return object.ParentElement() != nil &&
						object.Class().Contains("product-tip-slider-anchor-wrap")
				})
				if parent != nil {
					removeElement(parent)
				}
			} else if hasAnchorSiblings(i) {
				removeElement(i)
			} else if inDropMenu(i) {
				parent := queryParent(i, func(object dom.Element) bool {
					return object.TagName() == "LI"
				})
				if parent != nil {
					removeElement(parent)
				}
			} else if isInPopularCategory(i) {
				parent := queryParent(i, func(object dom.Element) bool {
					return object.TagName() == "LI"
				})
				if parent != nil {
					removeElement(parent)
				}
			}
		},
	)
}

func removeElement(o dom.Element) {
	parent := o.ParentElement()
	if parent != nil {
		parent.RemoveChild(o)
	}
}

func isApple(o dom.Element) bool {
	return strings.Contains("Apple", o.InnerHTML())
}

func isProductTipSliderAnchor(o dom.Element) bool {
	return queryParent(o, func(object dom.Element) bool {
		return object.Class().Contains("product-tip-slider-anchor")
	}) != nil
}

func inDropMenu(o dom.Element) bool {
	return queryParent(o, func(object dom.Element) bool {
		return object.Class().Contains("dropdown-menu")
	}) != nil
}

func isInPopularCategory(o dom.Element) bool {
	return queryParent(o, func(object dom.Element) bool {
		return object.Class().Contains("popular-categories-tile")
	}) != nil
}

func hasAnchorSiblings(o dom.Element) bool {
	prev := o.PreviousElementSibling()
	if prev != nil && prev.TagName() == "A" {
		return true
	}
	next := o.NextElementSibling()
	if next != nil && next.TagName() == "A" {
		return true
	}
	return false
}

func queryParent(o dom.Element, tester func(object dom.Element) bool) dom.Element {
	parent := o.ParentElement()
	depth := 0
	for parent != nil && depth < 15 {
		if tester(parent) {
			return parent
		}
		depth++
		parent = parent.ParentElement()
	}
	return nil
}

func forEach(o []dom.Element, callback func(i dom.Element)) {
	for _, elem := range o {
		callback(elem)
	}
}
